class KhmerDate {
    khmerNum : Array<string> = ['០','១','២','៣','៤','៥','៦','៧','៨','៩',];
    currDate: Date = new Date();
    date: Date;
    constructor(date: Date){
        this.date = date;
    }

    dateCompare(): number{
        if(this.currDate <= this.date){
            return 0;
        }else if(this.currDate > this.date){
            return (this.currDate.getTime() - this.date.getTime())/(1000);
        }
    }

    toKhmerNum(input: number): String{
        let khNum: String="";
        for(let i=0; i < String(input).length; i++){
            let index: number = parseInt(String(input).slice(i,i+1));
            khNum += this.khmerNum[index];
        }
        return khNum;
    }

    checkDate(): String{
        let seconds: number = this.dateCompare();
        if(seconds<60){ //smaller than 1 minute
            return "មុននេះបន្តិច"
        }else if(seconds < 3600){ //smaller than 1hour 
            let minutes: number = Math.floor(seconds/60);
            return this.toKhmerNum(minutes)+" នាទីមុន"
        }else if(seconds < 3600*24){ //smaller than 1day
            let hours: number = Math.floor(seconds/3600);
            return this.toKhmerNum(hours)+" ម៉ោងមុន"
        }else if(seconds < 3600*24*7){ //smaller than 1week
            let days: number = Math.floor(seconds/(3600*24));
            return this.toKhmerNum(days)+" ថ្ងៃមុន"
        }else if(seconds < 3600*24*30){ //smaller than 1month
            let weeks: number = Math.floor(seconds/(3600*24*7));
            return this.toKhmerNum(weeks)+" សប្ដាហ៏មុន"
        }else if(seconds > 3600*24*30){ //biger than 1 month
            let months: number = Math.floor(seconds/(3600*24*30));
            return this.toKhmerNum(months)+" ខែមុន"
        }
    }

    getDate(): String{
        return this.checkDate();
    }
}

export{
    KhmerDate
}