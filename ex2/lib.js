"use strict";
exports.__esModule = true;
exports.KhmerDate = void 0;
var KhmerDate = /** @class */ (function () {
    function KhmerDate(date) {
        this.khmerNum = ['០', '១', '២', '៣', '៤', '៥', '៦', '៧', '៨', '៩',];
        this.currDate = new Date();
        this.date = date;
    }
    KhmerDate.prototype.dateCompare = function () {
        if (this.currDate <= this.date) {
            return 0;
        }
        else if (this.currDate > this.date) {
            return (this.currDate.getTime() - this.date.getTime()) / (1000);
        }
    };
    KhmerDate.prototype.toKhmerNum = function (input) {
        var khNum = "";
        for (var i = 0; i < String(input).length; i++) {
            var index = parseInt(String(input).slice(i, i + 1));
            khNum += this.khmerNum[index];
        }
        return khNum;
    };
    KhmerDate.prototype.checkDate = function () {
        var seconds = this.dateCompare();
        if (seconds < 60) { //smaller than 1 minute
            return "មុននេះបន្តិច";
        }
        else if (seconds < 3600) { //smaller than 1hour 
            var minutes = Math.floor(seconds / 60);
            return this.toKhmerNum(minutes) + " នាទីមុន";
        }
        else if (seconds < 3600 * 24) { //smaller than 1day
            var hours = Math.floor(seconds / 3600);
            return this.toKhmerNum(hours) + " ម៉ោងមុន";
        }
        else if (seconds < 3600 * 24 * 7) { //smaller than 1week
            var days = Math.floor(seconds / (3600 * 24));
            return this.toKhmerNum(days) + " ថ្ងៃមុន";
        }
        else if (seconds < 3600 * 24 * 30) { //smaller than 1month
            var weeks = Math.floor(seconds / (3600 * 24 * 7));
            return this.toKhmerNum(weeks) + " សប្ដាហ៏មុន";
        }
        else if (seconds > 3600 * 24 * 30) { //biger than 1 month
            var months = Math.floor(seconds / (3600 * 24 * 30));
            return this.toKhmerNum(months) + " ខែមុន";
        }
    };
    KhmerDate.prototype.getDate = function () {
        return this.checkDate();
    };
    return KhmerDate;
}());
exports.KhmerDate = KhmerDate;
