const { KhmerDate } = require('./lib')
const date = new KhmerDate(new Date('2022-02-26'));
const date1 = new KhmerDate(new Date('2021-02-26'));
const date2 = new KhmerDate(new Date('2022-03-01'));
const date3 = new KhmerDate(new Date('2022-03-02'));
const date4 = new KhmerDate(new Date('2022-03-02T07:00:00Z'));
const date5 = new KhmerDate(new Date('2022-03-02T10:05:00Z'));
console.log(new Date()); //display current date
console.log(date.getDate());
console.log(date1.getDate());
console.log(date2.getDate());
console.log(date3.getDate());
console.log(date4.getDate());
console.log(date5.getDate());